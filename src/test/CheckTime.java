import time.mesure.Time;


/**
 * Created by Slava on 11/20/15.
 */
public class CheckTime {

    @Time(format="method time: %s ms")
    public static long echo(long param) throws Exception{
        Thread.sleep(500);
        System.out.println("echo " + param);
        return param;
    }

    @Time(format="method time: %s ms")
    public static void main(String a[]) throws Exception{

        Thread.sleep(2000);

        for (int i=0; i<5; i++)
            echo(10);
        System.out.println("done");
    }
}