package time.mesure;

/**
 * Created by Slava on 11/20/15.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface Time {
    public enum TimeInterval { MILLISECOND, NANOSECOND };
    Time.TimeInterval interval() default Time.TimeInterval.MILLISECOND;
    String format() default "Elapsed %s";
}