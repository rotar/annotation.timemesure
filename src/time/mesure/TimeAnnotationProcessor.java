package time.mesure;

/**
 * Created by Slava on 11/20/15.
 */
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;

import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

@SupportedAnnotationTypes(value = {TimeAnnotationProcessor.ANNOTATION_TYPE})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TimeAnnotationProcessor extends AbstractProcessor {

    public static final String ANNOTATION_TYPE = "time.mesure.Time";
    private JavacProcessingEnvironment javacProcessingEnv;
    private TreeMaker maker;

    static{System.out.println("Apply Time Annotation...");}

    @Override
    public void init(ProcessingEnvironment procEnv) {
        super.init(procEnv);
        this.javacProcessingEnv = (JavacProcessingEnvironment) procEnv;
        this.maker = TreeMaker.instance(javacProcessingEnv.getContext());
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        if (annotations == null || annotations.isEmpty()) {
            return false;
        }

        final Elements elements = javacProcessingEnv.getElementUtils();

        final TypeElement annotation = elements.getTypeElement(ANNOTATION_TYPE);

        if (annotation != null) {
            // Выбираем все элементы, у которых стоит наша аннотация
            final Set<? extends Element> methods = roundEnv.getElementsAnnotatedWith(annotation);

            JavacElements utils = javacProcessingEnv.getElementUtils();
            for (final Element m : methods) {
                Time time = m.getAnnotation(Time.class);
                if (time != null) {
                    JCTree blockNode = utils.getTree(m);
                    // Нам нужны только описания методов
                    if (blockNode instanceof JCMethodDecl) {
                        // Получаем содержимое заанотированого метода
                        final List<JCStatement> statements = ((JCMethodDecl) blockNode).body.stats;

                        // Новое тело метода
                        List<JCStatement> newStatements = List.nil();
                        // Добавляем в начало метода сохранение текущего времени
                        JCVariableDecl var = makeTimeStartVar(maker, utils, time); //var: "final long time_start_3343 = System.currentTimeMillis()"
                        newStatements = newStatements.append(var);

                       // Создаём тело блока try, копируем в него оригинальное содержимое метода
                        List<JCStatement> tryBlock = List.nil();
                        for (JCStatement statement : statements) {
                            tryBlock = tryBlock.append(statement);
                        }

                        // Создаём тело блока finally, добавляем в него вывод затраченного времени
                        JCBlock finalizer = makePrintBlock(maker, utils, time, var);
                        JCStatement stat = maker.Try(maker.Block(0, tryBlock), List.<JCCatch>nil(), finalizer); //build try-catch-final block
                        newStatements = newStatements.append(stat);

                        //System.out.print("New Method code is :" + newStatements);

                        // Заменяем старый код метода на новый
                        ((JCMethodDecl) blockNode).body.stats = newStatements;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private JCExpression makeCurrentTime(TreeMaker maker, JavacElements utils, Time time) {
        // Создаём вызов System.nanoTime или System.currentTimeMillis
        String methodName;
        switch (time.interval()) {
            case NANOSECOND:
                methodName = "nanoTime";
                break;
            default:
                methodName = "currentTimeMillis";
                break;
        }
        JCExpression exp = maker.Select(maker.Ident(utils.getName("System")), utils.getName(methodName));
        return maker.Apply(List.<JCExpression>nil(), exp, List.<JCExpression>nil()); //Syste.currentTimeMillis()
    }

    protected JCVariableDecl makeTimeStartVar(TreeMaker maker, JavacElements utils, Time time) {
        // Создаём финальную переменную для хранения времени старта. Имя переменной в виде time_start_{random}
        JCExpression currentTime = makeCurrentTime(maker, utils, time);
        String fieldName = "time_start_" + (int) (Math.random() * 10000);
        return maker.VarDef(maker.Modifiers(Flags.FINAL),
                                utils.getName(fieldName),
                                    maker.TypeIdent(TypeTag.LONG),
                                        currentTime); // return: final long time_start_8075 = System.currentTimeMillis()
    }

    protected JCBlock makePrintBlock(TreeMaker maker, JavacElements utils, Time time, JCVariableDecl var) {

        JCExpression printlnExpression = maker.Ident(utils.getName("System"));
        printlnExpression = maker.Select(printlnExpression, utils.getName("out"));
        printlnExpression = maker.Select(printlnExpression, utils.getName("println"));

        // Создаём блок вычисления затраченного времени (currentTime - startTime)
        //final long time_start_4912 = System.currentTimeMillis();
        //System.currentTimeMillis() - time_start_4912
        JCExpression currentTime = makeCurrentTime(maker, utils, time); //define currentTime: "final long time_start_4912 = System.currentTimeMillis()"
        JCExpression elapsedTime = maker.Binary(JCTree.Tag.MINUS, currentTime, maker.Ident(var.name)); // elapsedTime = "System.currentTimeMillis() - time_start_4912"
        //System.out.println(">>current time : " + currentTime.toString());
        //System.out.println(">>elapsed time : " + elapsedTime.toString());

        // Форматируем результат
        JCExpression formatExpression = maker.Ident(utils.getName("String"));
        formatExpression = maker.Select(formatExpression, utils.getName("format")); // create  "String.format"

        //System.out.println(">>format expression : " + formatExpression.toString());
        //System.out.println(">>time format : " + time.format());

        //FixMe
        // Собираем все кусочки вместе что бы получил формат времени
        //List<JCExpression> formatArgs = List.nil();
        //formatArgs.append(maker.Literal(time.format())); //time.format() = "method time: %s ms"
        //formatArgs.append(elapsedTime);

        //Fixed, use List.of instead of List.append method
        List<JCExpression> formatArgs = List.of(maker.Literal(time.format()), elapsedTime); // create str: "method time: %s ms",System.currentTimeMillis() - time_start_4912

        JCExpression format = maker.Apply(List.<JCExpression>nil(), formatExpression, formatArgs); // String.format("method time: %s ms", System.currentTimeMillis() - time_start_4912)

        //FixMe
        //List<JCExpression> printlnArgs = List.nil();
        //printlnArgs.append(format);

        //Fixed, use List.of instead of List.append method
        List<JCExpression> printlnArgs = List.of(format);//printlnArgs: System.out.println(String.format("method time: %s ms", System.currentTimeMillis() - time_start_4912))

        JCExpression print = maker.Apply(List.<JCExpression>nil(), printlnExpression, printlnArgs); //print: System.out.println(String.format("method time: %s ms", System.currentTimeMillis() - time_start_9956))
        JCExpressionStatement stmt = maker.Exec(print);

        //List<JCStatement> stmts = List.nil();
        //stmts.append(stmt);
        //FIX: use List.of instead of List.append method
        List<JCStatement> stmts = List.of(stmt);

        return maker.Block(0, stmts);
    }
}

