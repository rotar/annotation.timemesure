Annotation to calculate time of executing method.
original: http://special.habrahabr.ru/kyocera/p/88908/


How to run:

0. Precondition - build jar artifact to build timemesure.jar

1. Compile java code with sourcepath
cd ./annotation.timemesure/
javac -b bin/ -classpath out/artifacts/timemesure_jar/annotation.timemesure.jar src/test/CheckTime.java

1.1 add to your code to test time measure: 
import time.mesure.Time;
# add annotation to desired method 
@Time(format="testLogin method time: %s ms")

2. Launch compiled java class
cd bin/
java CheckTime

In responce you will get the time of executing annotated method.
> method time: 4541 ms


*************************

you can download the timemesure.jar file from this project by path ./lib/timemesure.jar